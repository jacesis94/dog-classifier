import os
import tarfile
import re
import urllib.request
import logging
import json

import numpy as np


class ImagesSetUp:

    def __init__(self):
        self.url = 'http://vision.stanford.edu/aditya86/ImageNetDogs/images.tar'
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)
        self.classes = None

    def download(self, name):
        if not os.path.exists(name):
            self.logger.info('Downloading file')
            urllib.request.urlretrieve(self.url, name)
        else:
            self.logger.warning('File already exists')

    def extract(self, source, target, name):
        if not os.path.exists(source):
            self.logger.info('Extracting images')
            with tarfile.open(name) as f:
                f.extractall(target)
        else:
            self.logger.warning('Images already extracted')

    @staticmethod
    def _create_dir(path):
        if not os.path.exists(path):
            os.mkdir(path)

    def generate_classes_dict(self):
        return {i: class_ for i, class_ in enumerate(self.classes)}

    def distribute(self, dir_, train_ratio):
        folders = os.listdir(dir_)
        train_path = os.path.join(dir_, 'train')
        val_path = os.path.join(dir_, 'val')

        self._create_dir(train_path)
        self._create_dir(val_path)

        for folder_name in folders:
            folder_path = os.path.join(dir_, folder_name)

            images = os.listdir(folder_path)
            num_images = len(images)

            train_images = [*np.random.choice(images,
                                              int(np.floor(train_ratio * num_images)),
                                              replace=False)]
            val_images = [*set(images) - set(train_images)]

            preprocessed_name = re.sub(r'n[\d]{8}-', '', folder_name). \
                replace('-', '_'). \
                lower()

            train_class_path = os.path.join(train_path, preprocessed_name)
            self._create_dir(train_class_path)

            for image in train_images:
                os.rename(os.path.join(folder_path, image),
                          os.path.join(train_class_path, image))

            val_class_path = os.path.join(val_path, preprocessed_name)
            self._create_dir(val_class_path)

            for image in val_images:
                os.rename(os.path.join(folder_path, image),
                          os.path.join(val_class_path, image))

            os.rmdir(folder_path)

        self.logger.info('Images distributed')
        self.classes = sorted(os.listdir(train_path))


if __name__ == "__main__":
    name_ = 'images.tar'
    base_dir = 'Images'
    train_ratio_ = 0.8

    images_su = ImagesSetUp()

    images_su.download(name=name_)

    images_su.extract(source=base_dir,
                      target='./',
                      name=name_)

    images_su.distribute(dir_=base_dir,
                         train_ratio=train_ratio_)

    classes = images_su.generate_classes_dict()

    with open('classes.json', 'w') as f:
        json.dump(classes, f)
