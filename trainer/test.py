import torch
from torchvision import transforms
import json

from PIL import Image

# open method used to open different extension image file
# img = Image.open('Images_test/labrador.jpg')
#
# # This method will show image in any image viewer
# img.show()
#
# transformations = transforms.Compose([
#         transforms.Resize(256),
#         transforms.CenterCrop(224),
#         transforms.ToTensor(),
#         transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
#     ])
#
# model = torch.load('model.pt', map_location=torch.device('cpu'))
# model.eval()
#
# with open('classes.json', 'r') as f:
#     classes = json.load(f)
#
# img_test = test_transforms(img)
# img_test = img_test.unsqueeze(0)
#
# outputs = torch.nn.Softmax(dim=1)(model(img_test))
# # _, preds = torch.max(outputs, dim=1)
#
# outputs = outputs.detach().numpy().flatten()
#
# pred_prob = {class_: prob for prob, class_ in zip(outputs, classes.values())}
# pred_prob = {k: v for k, v in sorted(pred_prob.items(),
#                                      key=lambda item: item[1],
#                                      reverse=True)}

# for prob, class_ in zip(outputs.detach().numpy().flatten(), classes.values()):
#     print('Class: {} - {:.4f} %'.format(class_, prob))
#
# pred_class = str(preds.item())
# print(classes[pred_class])


def prediction(model, image, transformations, classes):
    transformed_image = transformations(image)
    transformed_image = transformed_image.unsqueeze(0)

    outputs = torch.nn.Softmax(dim=1)(model(transformed_image))
    outputs = outputs.detach().numpy().flatten()

    pred_prob = {class_: prob for prob, class_ in zip(outputs, classes.values())}
    pred_prob = {k: v for k, v in sorted(pred_prob.items(),
                                         key=lambda item: item[1],
                                         reverse=True)}

    return pred_prob


if __name__ == '__main__':
    img = Image.open('Images_test/ratonero.jpeg')
    img.show()

    transformations = transforms.Compose([
        transforms.Resize(256),
        transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ])

    model = torch.load('model.pt', map_location=torch.device('cpu'))
    model.eval()

    with open('classes.json', 'r') as f:
        classes = json.load(f)

    prob_dict = prediction(model=model,
                           image=img,
                           transformations=transformations,
                           classes=classes)

    print(prob_dict)
