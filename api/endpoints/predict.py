import pathlib

from flask import Blueprint, request, jsonify
from flasgger import swag_from
from api.inference import Inference

predict_api = Blueprint('predict_api', __name__)

path = pathlib.Path(__file__).parent
path_utils = path.parent / 'utils'
path_model = path_utils / 'model.pt'
path_classes = path_utils / 'classes.json'

model = Inference(path_model=path_model,
                  path_classes=path_classes)


@predict_api.route('/predict', methods=['POST'])
@swag_from('swagger/predict.yml')
def predict():
    if request.method == 'POST':
        file = request.files.get('file')
        if not file:
            return
        img_bytes = file.read()
        name, prob = model.get_prediction(image_bytes=img_bytes)
        result = {'name': name,
                  'prob': prob}
        print(result)

        return jsonify(result)
