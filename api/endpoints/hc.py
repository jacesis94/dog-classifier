from flask import Blueprint
from flasgger import swag_from

hc_api = Blueprint('hc_api', __name__)


@hc_api.route('/hc', methods=['GET'])
@swag_from('swagger/hc.yml')
def health_check():
    return 'OK'
