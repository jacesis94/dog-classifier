import json
import io

import torch
from torchvision import transforms
from PIL import Image


class Inference:

    def __init__(self, path_model, path_classes):
        self.model = self._load_model(path_model)
        self.classes = self._load_classes(path_classes)

    @staticmethod
    def _load_model(path, device='cpu'):
        model = torch.load(path,
                           map_location=torch.device(device))
        model.eval()
        return model

    @staticmethod
    def _load_classes(path):
        with open(path, 'r') as f:
            classes = json.load(f)
        return classes

    @staticmethod
    def _transform_image(image_bytes):
        transformations = transforms.Compose([
            transforms.Resize(256),
            transforms.CenterCrop(224),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406],
                                 [0.229, 0.224, 0.225])
        ])
        image = Image.open(io.BytesIO(image_bytes))

        return transformations(image).unsqueeze(0)

    def get_prediction(self, image_bytes):
        image_tensor = self._transform_image(image_bytes=image_bytes)
        outputs = torch.nn.Softmax(dim=1)(self.model(image_tensor))
        pred_prob, pred_idx = outputs.max(dim=1)
        pred_class = self.classes[str(pred_idx.item())]

        return pred_class, pred_prob.item()
