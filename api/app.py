from flask import Flask
from flasgger import Swagger

from api.endpoints.predict import predict_api
from api.endpoints.hc import hc_api


app = Flask(__name__)
app.register_blueprint(predict_api, url_prefix='/api')
app.register_blueprint(hc_api, url_prefix='/api')

Swagger(app)

if __name__ == '__main__':
    app.run(host='0.0.0.0')
