# Dog classification API

## Configure server

Add domain to /etc/hosts file.
```bash
127.0.0.1 example.com
```

Forward HTTP request in router configuration.
```bash
PublicIP:80 to PrivateIP:80
```

Add record with server ip to DNS records.
```bash
example.com to PublicIP
```

## Usage

Build and run docker compose containers.
```bash
sudo docker-compose up --build
```
